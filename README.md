# pdo_kdb

#### 介绍
适配php的kingbase 的pdo扩展

#### 软件架构
软件架构说明


#### 安装教程

添加php.ini中的扩展
1-1、linux安装办法
    
1-2、windows 上安装KDB的pdo_kdb.so扩展办法
以linux环境为例，做如下说明：
  （1）下载地址  https://gitee.com/jinostart_admin_admin/pdo_kdb.git
  附录****注意：官网目前提供的大部分都是关闭php的Thread Safety选项的，如果测试环境开启了线程安全会加载不了驱动
下载地址：https://www.kingbase.com.cn/index/download/c_id/401.html
  （2）放置 /usr/local/php/lib/php/extensions/no-debug-non-zts-xxx/ 
   或 放置在自定义新建目录/opt/kdblib目录下
  （3）配置php.ini
    ; Directory in which the loadable extensions (modules) reside.
    ; http://php.net/extension-dir
    ; extension_dir = "./"
    ; On windows:
    ;添加扩展路径目录，和扩展文件路径

    extension_dir = "/opt/kdblib"
    extension=/opt/kdblib/pdo_kdb.so

    ;或者
    extension=/usr/local/php/lib/php/extensions/no-debug-non-zts-20180731/pdo_kdb.so
   （4）重启php 服务
    #service php-fpm restart
    #service nginx restart
    #service httpd restart
    (5)查看 PHP扩展
    # php -v
    # php -m  

#### 使用说明

1.  pdo_kdb_for_php7.3.15_nts-8.7.3-0001  为专用机使用 rmp安装包  主要适配信创环境 中科方德server
2.  v8r3_pdo_kdb_for_php-7.3.32_x86_64_linux  为 x86 linux 使用
3.  v8r3_pdo_kdb_for_php7.3.33_nts_win64     为 windows 64  使用

#### 参与贡献

1.  Charles Leekin

2、

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
