# pdo_kdb

#### Description
适配php的kingbase 的pdo扩展

#### Software Architecture
Software architecture description

#### Installation

添加php.ini中的扩展
1-1、linux安装办法
    
1-2、windows 上安装KDB的pdo_kdb.so扩展办法
以linux环境为例，做如下说明：
  （1）下载地址  https://gitee.com/jinostart_admin_admin/pdo_kdb.git
  附录****注意：官网目前提供的大部分都是关闭php的Thread Safety选项的，如果测试环境开启了线程安全会加载不了驱动
下载地址：https://www.kingbase.com.cn/index/download/c_id/401.html
  （2）放置 /usr/local/php/lib/php/extensions/no-debug-non-zts-xxx/ 
   或 放置在自定义新建目录/opt/kdblib目录下
  （3）配置php.ini
    ; Directory in which the loadable extensions (modules) reside.
    ; http://php.net/extension-dir
    ; extension_dir = "./"
    ; On windows:
    ;添加扩展路径目录，和扩展文件路径

    extension_dir = "/opt/kdblib"
    extension=/opt/kdblib/pdo_kdb.so

    ;或者
    extension=/usr/local/php/lib/php/extensions/no-debug-non-zts-20180731/pdo_kdb.so
   （4）重启php 服务
    #service php-fpm restart
    #service nginx restart
    #service httpd restart
    (5)查看 PHP扩展
    # php -v
    # php -m  

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
